@ECHO OFF
SET WORK_DIR=%~dp0
SET M2_CFG=%WORK_DIR:~0,-1%\gradle.cfg
SET GRADLE_CFG=%WORK_DIR:~0,-1%\maven.cfg

CALL :JVM_INIT "%ProgramFiles%\Java"

IF NOT EXIST %M2_CFG% TYPE NUL>%M2_CFG%
IF NOT EXIST %GRADLE_CFG% TYPE NUL>%GRADLE_CFG%

IF [%1] NEQ [] (
    IF "%1" EQU "alternative" (
        IF [%2] NEQ [] (
            IF EXIST %WORK_DIR:~0,-1%\%2 (
                IF [%3] NEQ [] (
                    IF "%3" EQU "list" CALL :LIST %WORK_DIR:~0,-1% %2
                    IF "%3" EQU "set" CALL :SET %WORK_DIR:~0,-1% %2 %4
                ) ELSE (
                    GOTO :NOTING
                )
            ) ELSE (
                GOTO :NOTING
            )
        ) ELSE (
            GOTO :NOTING
        )
    ) ELSE (
        IF "%1" EQU "-q" CALL :EXIT
        GOTO :EXIT
    )
) ELSE (
    GOTO :NOTING
)

REM IF BEFORE ACTIONS COMPLETE, EXIT.
GOTO :EXIT

:JVM_INIT

    SET _JAVA_HOME=%1

    IF NOT EXIST %_JAVA_HOME% (
        SET _JAVA_HOME="%ProgramFiles(x86)%\Java"
    )
    IF [%_JAVA_HOME%] EQU [] (
        ECHO JVM NOT FOUND!
        GOTO :EXIT
    )
    ECHO JVM FOUND ON %_JAVA_HOME%

    SET JVM_CFG=%WORK_DIR:~0,-1%\jvm.cfg
    IF NOT EXIST %JVM_CFG% TYPE NUL>%JVM_CFG%

    SET /P JVM_VER=<%JVM_CFG%
    IF ["%JVM_VER%"] EQU [""] (
        FOR /F "tokens=*" %%C IN ('DIR /A:D /B %_JAVA_HOME%') DO (
            ECHO [ ] %%C
            IF NOT EXIST %WORK_DIR:~0,-1%\jvm\%%C MD %WORK_DIR:~0,-1%\jvm\%%C
        )
        ECHO NO JVM ASSIGN IS FOUND!
        ECHO PLEASE EXECUTE: ^'update alternative jvm set ^<version^>^'
    ) ELSE (
        REM CLEAR CURRENT VALUE TO NEW SET
        ECHO "%_JAVA_HOME:"=%\%JVM_VER%">%WORK_DIR%\jvm\%JVM_VER%\tmp
        SET /P JAVA_HOME

        REM SET JRE_DIR=%JAVA_HOME%\jre
        REM SET JRE_BIN=%JAVA_HOME%\bin
    )

GOTO :EOF

:LIST
    FOR /F "tokens=*" %%D IN ('DIR /A:D /B %1\%2') DO (
        IF "%%D" EQU "%JVM_VER%" (
            ECHO [*] %%D
        ) ELSE (
            ECHO [ ] %%D
        )
    ) 
GOTO :EXIT

:SET
    IF "%2" NEQ "jvm" (
        ECHO 1
    ) ELSE (
        ECHO %3>%JVM_CFG%
        CALL :JVM_INIT
    )
GOTO :EXIT

REM IF INVALID ARG INPUT, SHOW MESSAGE AND EXIT
:NOTING
    ECHO PLEASE USE A VALID OPTION
GOTO :EXIT

:EXIT